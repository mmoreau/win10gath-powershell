<#
    Lists all connections and disconnections of users' sessions.

    This script can be executed without administrator rights.
#>
 
Get-EventLog System -Source Microsoft-Windows-WinLogon | Select @{Label="Datetime";Expression={$_.TimeGenerated}}, @{Label="SID";Expression={$_.replacementstrings[1]}}, @{Label="User";Expression={([String](New-Object System.Security.Principal.SecurityIdentifier $_.replacementstrings[1]).Translate([System.Security.Principal.NTAccount])).Split("\")[-1]}} ,@{Label="State";Expression={@("Logout","Login")[$_.EventID % 2]}}

# Filter only today
#Get-EventLog System -Source Microsoft-Windows-WinLogon -After (Get-Date).AddDays(-1) | Select @{Label="Datetime";Expression={$_.TimeGenerated}}, @{Label="SID";Expression={$_.replacementstrings[1]}}, @{Label="User";Expression={([String](New-Object System.Security.Principal.SecurityIdentifier $_.replacementstrings[1]).Translate([System.Security.Principal.NTAccount])).Split("\")[-1]}} ,@{Label="State";Expression={@("Logout","Login")[$_.EventID % 2]}}