<#
    By default Powershell does not allow access to a website with an untrusted SSL/TLS certificate.

    It is however possible to access it by proceeding as follows
#>

cls # Clean the console (not mandatory, you can remove it)

Add-Type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@

[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
[Net.ServicePointManager]::SecurityProtocol = "Ssl3, Tls, Tls11, Tls12, Tls13"

Invoke-WebRequest -Uri https://localhost:7300 -Method "GET" -Headers @{'User-Agent' = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:68.0) Gecko/20100101 Firefox/68.0'} -UseBasicParsing -ErrorAction SilentlyContinue