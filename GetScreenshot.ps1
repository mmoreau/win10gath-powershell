# This script allows you to take screenshots

# Adds a Microsoft .NET Core type (a class) to a PowerShell session. 

<# Way 1
[System.Reflection.Assembly]::LoadWithPartialName(“System.Windows.Forms”)
[System.Reflection.Assembly]::LoadWithPartialName(“System.Drawing”)
#>

# Way 2
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

function screenshot {

    # Take a screenshot of each screen one by one 

    [System.Windows.Forms.Screen]::AllScreens | Select WorkingArea, Bounds | ForEach {
        $bitmap = New-Object System.Drawing.Bitmap $_.Bounds.Width, $_.Bounds.Height
        $graphic = [System.Drawing.Graphics]::FromImage($bitmap)
        $graphic.CopyFromScreen($_.WorkingArea.Left, $_.WorkingArea.Top, 0, 0, $bitmap.Size)
        $bitmap.Save([Environment]::GetFolderPath("Desktop") + "\" +$(Get-Date | Select @{Name="Date";Expression={$($_.Year, $_.Month, $_.Day, $_.Hour, $_.Minute, $_.Second, $_.Millisecond) -join ''}}).Date + ".png")
        $graphic.Dispose()
        $bitmap.Dispose()
    }
}


function screenshot2 {

    # Take a screenshot of all screens at the same time

    $screen = [System.Windows.Forms.SystemInformation]::VirtualScreen
    $bitmap = New-Object System.Drawing.Bitmap $screen.Width, $screen.Height
    $graphic = [System.Drawing.Graphics]::FromImage($bitmap)
    $graphic.CopyFromScreen($screen.Left, $screen.Top, 0, 0, $bitmap.Size)
    $bitmap.Save([Environment]::GetFolderPath("Desktop") + "\" +$(Get-Date | Select @{Name="Date";Expression={$($_.Year, $_.Month, $_.Day, $_.Hour, $_.Minute, $_.Second, $_.Millisecond) -join ''}}).Date + ".png")
    $graphic.Dispose()
    $bitmap.Dispose()
}

# Call the function
screenshot