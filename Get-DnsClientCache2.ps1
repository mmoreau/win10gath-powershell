Get-DnsClientCache | Select Data, Entry, Name, TimeToLive

Get-CimInstance -Query "SELECT * FROM MSFT_DNSClientCache" -Namespace root/StandardCimv2 | Select Data, Entry, Name, TimeToLive