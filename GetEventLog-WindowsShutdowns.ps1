# Displays windows shutdowns

Get-EventLog System -Source user32 | Select @{Label="DateTime";Expression={$_.TimeGenerated}}, @{Label="User";Expression={$_.UserName.Split("\")[-1]}}