<#
    Displays the date and time of each Windows start 

    This script must be executed with administrator rights, without these rights you cannot display the information and indicates an error
#>

Get-EventLog Security -Source Microsoft-Windows-Security-Auditing | Where {$_.EventID -eq 4608} | Select @{Label="DateTime";Expression={$_.TimeGenerated}}