# https://support.microsoft.com/en-us/help/243330/well-known-security-identifiers-in-windows-operating-systems

Get-LocalGroupMember -Group (Get-LocalGroup | Where {$_.SID -Match "^S-1-5-32-544$"} | Select -ExpandProperty Name) | Where {$_.SID -Match "[0-9]{4}$"} | Select Name | ForEach {$_.Name.Split("\")[-1]}