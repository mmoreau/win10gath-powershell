# https://docs.microsoft.com/en-us/previous-versions/windows/desktop/stormgmt/msft-storagesubsystem
# https://docs.microsoft.com/en-us/previous-versions/windows/desktop/stormgmt/msft-storagesubsystem-getdiagnosticinfo

# Gathering Storage SubSystem Diagnostic Information

if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] “Administrator”)){
    Write-Output ''
    Write-Warning “Execute the script with administrator rights to start the dump”
} else
{
    Invoke-CimMethod -InputObject (Get-CimInstance -ClassName MSFT_StorageSubSystem -Namespace Root\Microsoft\Windows\Storage) -MethodName "GetDiagnosticInfo" -Arguments @{DestinationPath="C:\Users\Maxime\Desktop\"; IncludeLiveDump=$true} -ErrorAction SilentlyContinue | Out-Null
}
