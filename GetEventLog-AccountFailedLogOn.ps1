<#
    Displays account login failures

    By executing this script two informations will be displayed :

    - DateTime : Displays the date and time
    - User : Tentative / Login failure on this user

    This script must be executed with administrator rights, without these rights you cannot display the information and indicates an error
#>

Get-EventLog Security -Source Microsoft-Windows-Security-Auditing | Where {$_.EventID -eq 4625} | Select @{Label="DateTime";Expression={$_.TimeGenerated}}, @{Label="User"; Expression={$_.ReplacementStrings[5]}}

# Filter only today
#Get-EventLog Security -Source Microsoft-Windows-Security-Auditing -After (Get-Date).AddDays(-1) | Where {$_.EventID -eq 4625} | Select @{Label="DateTime";Expression={$_.TimeGenerated}}, @{Label="User"; Expression={$_.ReplacementStrings[5]}}