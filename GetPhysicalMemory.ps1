Get-CimInstance win32_physicalmemory | Select Manufacturer, Banklabel, ConfiguredClockSpeed, @{Name="Capacity"; Expression={$_.Capacity/1GB}}, DeviceLocator, SerialNumber

#Get-WmiObject win32_physicalmemory | Select Manufacturer, Banklabel, ConfiguredClockSpeed, @{Name="Capacity"; Expression={$_.Capacity/1GB}}, DeviceLocator, SerialNumber