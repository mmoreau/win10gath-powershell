if (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] “Administrator”)){
    
    Get-Process -IncludeUserName | 
        Where {$_.UserName -eq $(Get-WmiObject -Class Win32_ComputerSystem | Select -ExpandProperty username) -and $_.Company -ne "Microsoft Corporation"} | 
        Format-Table -AutoSize -Wrap UserName, Company, ProcessName, StartTime, Path
}

# .:: What are the processes that use a particular DLL ? ::.
function dll()
{
    # C:\Windows\System32\mscoree.dll

    #Get-Process | Where {"C:\Windows\System32\kernel32.dll" -in $_.Modules.FileName} | Select Id, Name, StartTime
    Get-Process | Where {"kernel32.dll" -in $_.Modules.ModuleName} | Select Id, Name, StartTime
}


# .:: Hash the paths of the process executable ::.
function hash()
{
    Get-Process | Where {$_.Path} | Get-Unique -AsString | Format-Table -AutoSize -Wrap Name, Path, @{Name="Hash"; Expression={Get-FileHash $_.Path -Algorithm SHA256 | Select -ExpandProperty Hash}}
}


# .:: Displays all modules loaded by a process ::.
function modules()
{
    #(Get-Process | Where {$_.ProcessName -Match "^firefox"}  | Select Modules).Modules | Where {$_.FileName -Match "Mozilla Firefox"} | Sort FileName | Select -Unique FileName, BaseAddress, EntryPointAddress
    #(Get-Process | Where {$_.ProcessName -Match "^firefox"}  | Select Modules).Modules | Select -Unique FileName, BaseAddress, EntryPointAddress
    #(Get-Process | Where {$_.ProcessName -Match "^firefox"}  | Select Modules).Modules
    (Get-Process -Id 8620 | Select Modules).Modules | Select -Unique FileName, BaseAddress, EntryPointAddress
    #(Get-Process -Id 20880 | Select Modules).Modules | Select ModuleName
}


# .:: Lists all processes of the current user ::.
function currentUser()
{
    # Way 1
    <#
    $owners = @{}
    gwmi win32_process | % {$owners[$_.handle] = $_.getowner().user}
    Get-Process | Select ProcessName, Id, @{l="Owner"; e={$owners[$_.id.tostring()]}} | Where {$_.Owner -eq $env:UserName}
    #>

    # Way 2 
    #gwmi win32_process | ForEach { if($_.getowner().user -eq $env:UserName) { Write-Host $($_.ProcessName + " # " + $_.ProcessId) }}

    # Way 3
    #Get-WmiObject Win32_Process | Select ProcessName, ProcessId, @{Label="isCurrentUser"; Expression={$_.getowner().user -eq $env:UserName}} | Where {$_.isCurrentUser} | Select ProcessName, ProcessId

    # Way 4
    Get-WmiObject Win32_Process | Where {$_.GetOwner().User -eq $env:UserName} | Select ProcessId, ProcessName
}


# .:: Lists the processes that started a command line ::.
function commandLine()
{
    Get-WmiObject Win32_Process | Where {$_.CommandLine} | Select ProcessId, ProcessName, CommandLine
}


# .:: Filters by displaying only process execution paths ::.
function path()
{
     Get-WmiObject Win32_Process | Where {$_.Path} | Select ProcessId, ProcessName, Path
}

#dll
#hash
#modules
currentUser
#commandLine
#path