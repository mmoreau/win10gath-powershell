<#
    Source :
        https://docs.microsoft.com/en-us/dotnet/api/microsoft.powershell.commands.netconnectionstatus?view=powershellsdk-1.1.0
        https://docs.microsoft.com/en-us/windows/desktop/cimwin32prov/win32-networkadapter
        https://devblogs.microsoft.com/scripting/use-powershell-to-identify-your-real-network-adapter/
#>

#Displays the physical adapters that are true 
Get-WmiObject -Class Win32_NetworkAdapter | Where {$_.PhysicalAdapter -eq "True"}| Select ServiceName, Manufacturer, MacAddress, Name, AdapterType, PhysicalAdapter, NetConnectionId, NetEnabled, PNPDeviceID, GUID, NetConnectionStatus

#Displays the physical adapters that are true and connected
#Get-WmiObject -Class Win32_NetworkAdapter | Where {$_.PhysicalAdapter -eq "True" -and $_.NetConnectionStatus -eq "2"}| Select ServiceName, Manufacturer, MacAddress, Name, AdapterType, PhysicalAdapter, NetConnectionId, NetEnabled, PNPDeviceID, GUID, NetConnectionStatus