# slmgr /dlv => Execute with administrator rights, 
# The command line below does not need to be run as an administrator

Get-CimInstance -ClassName SoftwareLicensingProduct | Where { $_.LicenseFamily -eq "Core" -AND $_.PartialProductKey -ne $null} | Select Name, Description, ID, ApplicationID, OfflineInstallationId, PartialProductKey, ProductKeyChannel, ProductKeyID, ProductKeyID2, RemainingAppReArmCount, RemainingSkuReArmCount, UseLicenseURL, ValidationURL, TrustedTime, LicenseFamily