# Retrieve the Wallpaper on the current user's desktop
Get-ItemProperty "HKCU:\Control Panel\Desktop\" -ErrorAction SilentlyContinue | Select -ExpandProperty Wallpaper

# Recovers the Wallpaper on the desktop of all users => (Run with administrator rights)
Get-ChildItem -Path "Registry::HKEY_USERS" -ErrorAction SilentlyContinue | Where {$_.Name -Match '[0-9]{4}$'} | ForEach { Get-ItemProperty -Path ("Registry::" + $_.Name + "\Control Panel\Desktop") | Select -ExpandProperty Wallpaper}