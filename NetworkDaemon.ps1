if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] “Administrator”)){
    
    Write-Output ''
    Write-Warning “Run the script as administrator to get more information”
} 

Write-Output "`n.:: TCP ::."
Get-NetTCPConnection | 
    Select LocalAddress, 
           LocalPort, 
           RemoteAddress,
           RemotePort,
           @{Name="PID"; Expression={ $_.OwningProcess }},
           @{Name="UserName"; Expression={ (Get-Process -IncludeUserName -ID $_.OwningProcess -ErrorAction Continue) | Select -ExpandProperty UserName }}, 
           @{Name="ProcessName"; Expression={ (Get-Process -ID $_.OwningProcess).ProcessName }}, 
           @{Name="Path"; Expression={(Get-Process -ID $_.OwningProcess).Path }} |
    Format-Table -AutoSize -Wrap
    

Write-Output ".:: UDP ::."
Get-NetUDPEndpoint | 
    Select LocalAddress, 
           LocalPort,  
           RemoteAddress,
           RemotePort,
           @{Name="PID"; Expression={ $_.OwningProcess }},
           @{Name="UserName"; Expression={ (Get-Process -IncludeUserName -ID $_.OwningProcess -ErrorAction Continue) | Select -ExpandProperty UserName }}, 
           @{Name="ProcessName"; Expression={ (Get-Process -ID $_.OwningProcess).ProcessName }}, 
           @{Name="Path"; Expression={(Get-Process -ID $_.OwningProcess).Path }} |
    Format-Table -AutoSize -Wrap