<#
    Displays execution attempts as administrator

    When you request / execute a program as administrator, a new event is included in the log system

    This script must be executed with administrator rights, without these rights you cannot display the information and indicates an error
#>

Get-EventLog Security -Source Microsoft-Windows-Security-Auditing | Where {$_.EventID -eq 4672} | Select @{Label="DateTime";Expression={$_.TimeGenerated}}