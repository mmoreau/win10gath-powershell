function active_debug() {
    
    # Displays the configuration of network adapters that are enabled

    Get-WmiObject -Class Win32_NetworkAdapterConfiguration | Where {$_.IPEnabled -like "True" -and $_.DNSServerSearchOrder -notlike ""} | Select Description, IPAddress, IPSubnet, DefaultIPGateway, MacAddress, DNSDomain, DNSDomainSuffixSearchOrder, DNSServerSearchOrder, DHCPServer, DHCPEnabled, InterfaceIndex, SettingID | ForEach {
     
        Write-Host "Description                 :", $_.Description

        Write-Host "IPAddress / IPSubnet        :"
        $ipsubnet = $_.IPSubnet
        $ipsubnet_count = 0
        $_.IPAddress | ForEach {
            Write-Host "`t`t`t`t`t`t`t ", $_, "/", $ipsubnet[$ipsubnet_count]
            ++$ipsubnet_count

        }

        Write-Host "`nDefaultIPGateway           :"
        $_.DefaultIPGateway | ForEach {
            Write-Host "`t`t`t`t`t`t`t", $_
        }

        Write-Host "`nMacAddress                 :", $_.MacAddress
            
        Write-Host "`nDNSDomain                  :", $_.DNSDomain

        Write-Host "`nDNSDomainSuffixSearchOrder :"
        $_.DNSDomainSuffixSearchOrder | ForEach {
            Write-Host "`t`t`t`t`t`t`t", $_
        }

        Write-Host "`nDNSServerSearchOrder       :"
        $_.DNSServerSearchOrder | ForEach {
            Write-Host "`t`t`t`t`t`t`t", $_
        }

        Write-Host "`nDHCPServer                 :", $_.DHCPServer

        Write-Host "`nDHCPEnabled                :", $_.DHCPEnabled

        $SettingID = $_.SettingID
         
        Write-Host "`nInterfaceIndex             :", $(gwmi win32_networkadapter | Where {$_.GUID -Match $SettingID} | Select -ExpandProperty NetConnectionID)

        Write-Host "`n-------------------------------------------------------------------------`n"
     }
 }


 function active_json {
    Get-WmiObject -Class Win32_NetworkAdapterConfiguration | Where {$_.IPEnabled -like "True" -and $_.DNSServerSearchOrder -notlike ""} | Select Description, IPAddress, DefaultIPGateway, MacAddress, DNSDomain, DNSDomainSuffixSearchOrder, DNSServerSearchOrder, DHCPServer, DHCPEnabled, InterfaceIndex | ConvertTo-JSON
 }

 #call the function
 active_debug
 #active_json